-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2014 at 10:33 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `laravel_test_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Category 1', 3, '2014-08-15 16:39:30', '2014-08-15 19:45:19'),
(2, 'Category 2', 4, '2014-08-15 16:39:30', '2014-08-15 19:44:50'),
(3, 'Category 3', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(4, 'Category 4', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(5, 'Category 5', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(6, 'Category 6', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(7, 'Category 7', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(8, 'Category 8', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(9, 'Category 9', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(10, 'Category 10', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(11, 'Category 11', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(12, 'Category 12', 0, '2014-08-15 16:39:30', '2014-08-15 16:39:30'),
(13, 'Sample', 11, '2014-08-15 18:06:12', '2014-08-15 18:06:12'),
(14, 'Sample', 11, '2014-08-15 18:06:39', '2014-08-15 18:06:39'),
(15, 'Samlpe 2', 17, '2014-08-15 18:08:02', '2014-08-15 19:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_08_15_233822_create_categories_table', 1),
('2014_08_16_051318_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `categories`, `created_at`, `updated_at`) VALUES
(1, 'Title asdasd mEIDT', 'Body some asdasd EDIT', '1,2,3', '2014-08-15 22:47:48', '2014-08-15 23:50:13'),
(2, 'Title EDIT', 'Body some EIDT', '1,2,6,7', '2014-08-15 23:10:33', '2014-08-15 23:46:29'),
(3, 'Title', 'Body some', '1,2,3,4', '2014-08-15 23:11:38', '2014-08-15 23:49:17'),
(4, 'jghjghj', 'ghjhgj', '1,2,3,4,7,8', '2014-08-15 23:11:57', '2014-08-15 23:49:26'),
(5, 'asdasd', 'asdasdasd', '1,6,9', '2014-08-15 23:13:15', '2014-08-15 23:49:33'),
(6, 'Sample Title sometig', 'asdasdasd BOdy', '1,2,3', '2014-08-15 23:48:53', '2014-08-15 23:48:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
