<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$Categories = [
			['name' => 'Category 1', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 2', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 3', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 4', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 5', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 6', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 7', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 8', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 9', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 10', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 11', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],
			['name' => 'Category 12', 'created_at' => new DateTime, 'updated_at' =>  new DateTime],

		];


		DB::table('Categories')->insert($Categories);
	}

}