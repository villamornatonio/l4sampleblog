@extends('layout.master')
@section('content')

    <!-- Confirmation -->
    @include('popups.delete_confirmation')
    <!-- ./ Confirmation -->

	<div class="row-fluid">
		<h4>Posts</h4>

		<a href="{{ URL::to('posts/create') }}"  class="btn btn-info" ><span class="glyphicon glyphicon-plus"></span>New Post</a>
		
		<div class="table-responsive">
			<table class="table table-hover ">
				<thead>
					<tr>
						<th>Post ID</th>
						<th>Title</th>
						<th>Body</th>
						<th>Created</th>
						<th>Updated</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($posts as $post)
					<tr>
						<td>{{$post->id}}</td>
						<td>
							<a href="{{ URL::to("posts/$post->id") }}">{{$post->title}}</a>	
						</td>
						<td>{{$post->body}}</td>
						<td>{{date("F j, Y, g:i a",strtotime($post->created_at)) }}</td>
						<td>{{date("F j, Y, g:i a",strtotime($post->updated_at)) }}</td>
						<td>
							<a href='{{URL::to("posts/$post->id/edit")}}'><span class="glyphicon glyphicon-edit"></span></a>
							<a data-href="{{ route('posts.destroy',array($post->id)) }}" data-toggle="modal" data-target="#confirm-delete" href="#"><span class='glyphicon glyphicon-trash'></span></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row-fluid  text-center">

		{{$posts->links()}}
		
	</div>

@stop