@extends('layout.master')
@section('content')
	
	<div class="row-fluid">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			{{Form::open(array('url' => "posts/$post->id", 'method' => 'put'))}}
				<legend>Update Post</legend>
			
				<div class="form-group">
					{{Form::label('Title')}}
					{{Form::text('title', "$post->title", array('class' => 'form-control' , 'placeholder' => 'Title'))}}
				</div>
				@if($errors->has('title'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>{{$errors->first('title')}} </strong>
					</div>
				@endif
				<div class="form-group">
					{{Form::label('Body')}}
					{{Form::textarea('body', "$post->body", array('class' => 'form-control' , 'placeholder' => 'Body'))}}
				</div>
				@if($errors->has('body'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>{{$errors->first('body')}} </strong>
					</div>
				@endif
				<div class="form-group">
					{{Form::label('Category')}}
					@foreach ($categories as $key => $category)
						<div class="checkbox">
							<label>
								<input type="checkbox" <?php echo (in_array($key, explode(',',$post->categories))) ? 'checked' : '';?> name="categories[]" value="{{$key}}">
								{{$category}}
							</label>
						</div>
					@endforeach

				</div>

				<button type="submit" class="btn btn-primary">Update Post</button>
			{{Form::close()}}
			
		</div>
	</div>

	<div class="clearfix">
	
	</div>

@stop