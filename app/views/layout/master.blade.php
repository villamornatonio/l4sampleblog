
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{{ asset('assets/ico/favicon.png') }}}">

    <title>
      @section('title')
        Laravel 4 Sample Blog
      @show
    </title>

    <!-- Bootstrap core CSS -->
    <link href="{{{ asset('assets/css/bootstrap.css') }}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{{ asset('assets/css/style.css') }}}" rel="stylesheet">



    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container content-wrap">
      <!-- Notifications -->
      @include('popups.notification')
      <!-- ./ notifications -->
      
      <div class="header-container">
        <a href="{{{ URL::to('/') }}}" class="logo-link">
          <img src="{{ asset('assets/img/logo.jpg') }}" alt="" class="img-responsive">
        </a>
      </div>

      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="{{{ URL::to('/categories') }}}">Categories</a></li>
              <li><a href="{{{ URL::to('/posts') }}}">Posts</a></li>
            </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>

      <!-- Content -->
      @yield('content')
      <!-- ./ content -->

      
      <footer>
        <p>© Company 2014</p>
      </footer>
      <hr>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{{ asset('assets/js/jquery.v1.8.3.min.js') }}}"></script>
    <script src="{{{ asset('assets/js/bootstrap.min.js') }}}"></script>
    <script src="{{{ asset('assets/js/js.js') }}}"></script>
  </body>
</html>

<script type="text/javascript">
  $(document).ready(function(){
    <?php $notification = Session::get('notification'); ?>
    @if (isset($notification))
      popupbox.notification('popup-notification', '<p>{{$notification}}</p>' ,'{{$notification}}');
    @endif
  });  
</script>


