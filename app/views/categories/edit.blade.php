@extends('layout.master')
@section('content')
	
	<div class="row-fluid">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			{{Form::open(array('url' => "categories/$category->id", 'method' => 'put'))}}
				<legend>Edit Category</legend>
			
				<div class="form-group">
					{{Form::label('Category Name')}}
					{{Form::text('name', "$category->name", array('class' => 'form-control' , 'placeholder' => 'Category Name'))}}
				</div>
				@if($errors->has('name'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>{{$errors->first('name')}} </strong>
					</div>
				@endif

				<div class="form-group">
					{{Form::label('Parent Category')}}
					{{Form::select('parent', $categories,"$category->parent_id",array('class' => 'form-control'))}}
				</div>
				@if($errors->has('parent'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>{{$errors->first('parent')}} </strong>
					</div>
				@endif


				<button type="submit" class="btn btn-primary">Update Category</button>
			{{Form::close()}}
			
		</div>
	</div>

	<div class="clearfix">
	
	</div>

@stop