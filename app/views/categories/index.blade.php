@extends('layout.master')
@section('content')

    <!-- Confirmation -->
    @include('popups.delete_confirmation')
    <!-- ./ Confirmation -->

	<div class="row-fluid">
		<h4>Categories</h4>

		<a href="{{ URL::to('categories/create') }}"  class="btn btn-info" ><span class="glyphicon glyphicon-plus"></span>New Category</a>
		
		<div class="table-responsive">
			<table class="table table-hover ">
				<thead>
					<tr>
						<th>Category ID</th>
						<th>Name</th>
						<th>Parent Category</th>
						<th>Created</th>
						<th>Updated</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{$category->id}}</td>
						<td>
							<a href="{{ URL::to("categories/$category->id") }}">{{$category->name}}</a>
						</td>
						<td><?php echo ($category->parent_id != "") ? Category::find($category->parent_id)->name : 'NONE'; ?></td>
						<td>{{date("F j, Y, g:i a",strtotime($category->created_at)) }}</td>
						<td>{{date("F j, Y, g:i a",strtotime($category->updated_at)) }}</td>
						<td>
							<a href='{{URL::to("categories/$category->id/edit")}}'><span class="glyphicon glyphicon-edit"></span></a>
							<a data-href="{{ route('categories.destroy',array($category->id)) }}" data-toggle="modal" data-target="#confirm-delete" href="#"><span class='glyphicon glyphicon-trash'></span></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row-fluid  text-center">

		{{$categories->links()}}
		
	</div>

@stop