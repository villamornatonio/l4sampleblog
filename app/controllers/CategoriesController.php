<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
		//get all Categories on DB
		$categories = Category::paginate(5);
		//pass Categories of 5
		return View::make('categories.index')->with(compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//get selected columnds
		$cats = Category::all(array('id', 'name'));
		//iterate for array arangement
		foreach ($cats as $cat) {
			//set array 
			$categories[$cat->id] = $cat->name;
		}
		//pass and render array to view
		return View::make('categories.create')->with(compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		//set rules for fields
		$rules = array(
			'name'            => 'Required|max:50',
		);

		// Get all the inputs.
		$inputs = Input::all();

		// Validate the inputs.
		$validator = Validator::make($inputs, $rules);

		// Check if the form validates with success.
		if ($validator->passes()){
			// Create the category.
			$category = new Category;
			$category->name 	= Input::get('name');
			$category->parent_id  = Input::get('parent');
			$category->save();

			// Redirect to the User Index page.
			return Redirect::to('categories')->with('notification', 'Added a Category');
		}

		return Redirect::back()->withInput($inputs)->withErrors($validator->getMessageBag());
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//get post that has the category id
		$posts = Post::where('categories', 'LIKE', "%$id%")->paginate(5);
		//paginate and render array to view
		return View::make('categories.show')->with(compact('posts'));	
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//get all categories
		$cats = Category::all(array('id', 'name'));
		foreach ($cats as $cat) {
			//skip same id as Present Category
			if($cat->id != $id){
				//arrange in a array
				$categories[$cat->id] = $cat->name;
			}
			
		}
		$category = Category::find($id);
		return View::make('categories.edit')->with(compact('category'))->with(compact('categories'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//set rules
		$rules = array(
			'name'            => 'Required|max:50',
		);

		// Get all the inputs.
		$inputs = Input::all();

		// Validate the inputs.
		$validator = Validator::make($inputs, $rules);

		// Check if the form validates with success.
		if ($validator->passes()){
			// Create the Category.
			$category = Category::find($id);
			$category->name 	= Input::get('name');
			$category->parent_id  = Input::get('parent');
			$category->save();

			// Redirect to the User Index page.
			return Redirect::to('categories')->with('notification', 'Updated a Category');
		}

		return Redirect::back()->withInput($inputs)->withErrors($validator->getMessageBag());
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//get single category from DB
		$category = Category::find($id);
		if($category->delete()){
			// Redirect to the categories Index page.
			return Redirect::to('categories')->with('notification', "Category Successfuly Deleted!");
		}else{
			// Redirect to the categories Index page.
			return Redirect::to('categories')->with('notification', "Delete Not Successful");
		}
	}

}