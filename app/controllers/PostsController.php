<?php

class PostsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /posts
	 *
	 * @return Response
	 */
	public function index()
	{
		//get posts of DB on 5
		$posts = Post::paginate(5);
		//paginate and render posts on view
		return View::make('posts.index')->with(compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /posts/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//get all category with selected columns on DB
		$cats = Category::all(array('id', 'name'));
		foreach ($cats as $cat) {
			$categories[$cat->id] = $cat->name;
		}
		//render category on view
		return View::make('posts.create')->with(compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /posts
	 *
	 * @return Response
	 */
	public function store()
	{
		//set validate rules
		$rules = array(
			'title'            => 'Required|max:50',
			'body'            => 'Required|max:80',
		);

		// Get all the inputs.
		$inputs = Input::all();

		// Validate the inputs.
		$validator = Validator::make($inputs, $rules);

		// Check if the form validates with success.
		if ($validator->passes()){
			// Create the post.
			$post = new Post;
			$post->title 	= Input::get('title');
			$post->body  = Input::get('body');
			$post->categories  = implode(',', Input::get('categories'));
			$post->save();

			// Redirect to the User Index page.
			return Redirect::to('posts')->with('notification', 'Added a Post');
		}

		return Redirect::back()->withInput($inputs)->withErrors($validator->getMessageBag());
	}

	/**
	 * Display the specified resource.
	 * GET /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//get single post from DB
		$post = Post::find($id);
		//render post on view
		return View::make('posts.show')->with(compact('post'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /posts/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//get selected fields from DB
		$cats = Category::all(array('id', 'name'));
		foreach ($cats as $cat) {
			$categories[$cat->id] = $cat->name;
		}
		//get single post from DB
		$post = Post::find($id);
		//render post on view
		return View::make('posts.edit')->with(compact('categories'))->with(compact('post'));

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//set validation rules
		$rules = array(
			'title'            => 'Required|max:50',
			'body'            => 'Required|max:80',
		);

		// Get all the inputs.
		$inputs = Input::all();

		// Validate the inputs.
		$validator = Validator::make($inputs, $rules);

		// Check if the form validates with success.
		if ($validator->passes()){
			// Create the post.
			$post = Post::find($id);
			$post->title 	= Input::get('title');
			$post->body  = Input::get('body');
			$post->categories  = implode(',', Input::get('categories'));
			$post->save();

			// Redirect to the User Index page.
			return Redirect::to('posts')->with('notification', 'Updated a Post');
		}

		return Redirect::back()->withInput($inputs)->withErrors($validator->getMessageBag());
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		if($post->delete()){
			// Redirect to the posts Index page.
			return Redirect::to('posts')->with('notification', "Post Successfuly Deleted!");
		}else{
			// Redirect to the posts Index page.
			return Redirect::to('posts')->with('notification', "Delete Not Successful");
		}
	}

}