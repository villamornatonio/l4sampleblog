/**
* @author  - Villamor Natonio Jr
* @ www.puppysoftware.com
* @version - 1.0
*/

$(document).ready(function(){

	
	/* ================================================= UTILITIES =======================================================*/ 

    /**
    * Utility Object - Get Base URL, Image URL, Image Loader URL etc. 
    */
    util = new Object({
        //get base url 
        getBaseURL: function () {

            return location.protocol + "//" + location.host + '/';

        },
        //get image url /
        getImageURL: function () {
            return this.getBaseURL() + "img/";
        },
        //get image loader url 
        getImageLoadURL: function () {
            return this.getImageURL() + "ajax-loader.gif";
        },
        //get image pre loader url 
        getPreloader: function () {
            return this.getImageURL() + "loading.gif";
        }, 
        //get image error url 
        getImageErrURL: function () {
            return this.getImageURL() + "error-img.jpg";
        }
    });

    /* ================================================= POPUPS =======================================================*/ 

    popupbox = {

        notification: function (id, content, title) {
            id = typeof id != 'undefined' ? id : '';
            content = typeof content != 'undefined' ? content : '';
            title = typeof title != 'undefined' ? title : '';
            // $("h4#popup-modal-heading").text(title);
            $("#" + id + " .notifcation-content").html(content);
            $('#' + id).modal('show');
        }

    };
    /* ================================================= POPUPS =======================================================*/ 

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.danger').attr('href', $(e.relatedTarget).data('href')).attr('data-method', 'delete');

        $('[data-method]').append(function(){
            return "\n"+
            "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>\n"+
            "   <input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>\n"+
            "</form>\n"
        })
        .removeAttr('href')
        .attr('style','cursor:pointer;')
        .attr('onclick','$(this).find("form").submit();');
    });
    /* ================================================= RESTFULIZE =======================================================*/
    // $(function(){
    //     $('[data-method]').append(function(){
    //         return "\n"+
    //         "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>\n"+
    //         "   <input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>\n"+
    //         "</form>\n"
    //     })
    //     .removeAttr('href')
    //     .attr('style','cursor:pointer;')
    //     .attr('onclick','$(this).find("form").submit();');
    // });
    /* ================================================= RESTFULIZE =======================================================*/



});